//
//  ho.swift
//  secretRoom
//
//  Created by Oleg Liakh on 16.02.22.
//

import Foundation
import UIKit
class CustomImageView : UIImageView {
    var name: String = ""
    
   func configure(name: String){
        self.name = name
       self.image = loadImage(fileName: self.name)
    }
}

