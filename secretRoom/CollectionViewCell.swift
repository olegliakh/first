//
//  CollectionViewCell.swift
//  secretRoom
//
//  Created by Oleg Liakh on 14.02.22.
//

import UIKit

class CollectionViewCell: UICollectionViewCell {
     
   
    @IBOutlet weak var myImage: UIImageView!
    
    func configure( with photo: Photo ){
        myImage.image = loadImage(fileName: photo.name)
        myImage.isUserInteractionEnabled = false
    }
    
   
}
