//
//  CustomAlert.swift
//  secretRoom
//
//  Created by Oleg Liakh on 18.01.22.
//

import Foundation
import UIKit
class CustomAlert: UIView {
    
    @IBOutlet weak var blurView: UIVisualEffectView!
    @IBOutlet weak var alertLabel: UILabel!
    @IBOutlet weak var alertView: UIView!
    @IBOutlet weak var alertButton: UIButton!
    
    static func instanceFromNib() -> CustomAlert {
        guard let view = UINib(nibName: "CustomAlert", bundle: nil).instantiate(withOwner: nil, options: nil).first as? CustomAlert
        else {
            return CustomAlert()
        }
        return view
    }
    
    func showAlertWith(message: String, andTitle: String){
//        alertView.roundCorners(15)
        alertLabel.text = message
        alertButton.setTitle(andTitle, for: .normal)
    }
    
    @IBAction func alertButtonPressed(_ sender: UIButton) {
        removeFromSuperview()
    }
    
    
}
