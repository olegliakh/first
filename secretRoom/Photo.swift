//
//  Photo.swift
//  secretRoom
//
//  Created by Oleg Liakh on 17.01.22.
//

import Foundation

// MARK: - class
class Photo: Codable {
    
    
    // MARK: - lets/vars
    var name: String
    var description : String?
    var like : Bool = false
    init(name: String, description: String?, like: Bool ) {
        self.name = name
        self.description = description
    }
    private enum CodingKeys: String, CodingKey {
        case name
        case description
        case like
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(name, forKey: .name)
        try container.encode(description, forKey:.description )
        try container.encode(like, forKey: .like)
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.name = try container.decode(String.self, forKey: .name)
        self.description = try  container.decodeIfPresent(String.self, forKey: .description)
        self.like = try  container.decode(Bool.self, forKey: .like)
    }
    
}


