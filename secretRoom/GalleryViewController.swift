//
//  GalleryViewController.swift
//  secretRoom
//
//  Created by Oleg Liakh on 17.01.22.
//

import UIKit
// MARK: - class
class GalleryViewController: UIViewController {
    
    
    // MARK: - lets/vars
    var currentName = ""
    var currentIndex = 0
    var newImageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 0, height: 0  ))
    var photoArray: [Photo] = []
    var rotationAngle = CGFloat()
    let pickerSlider = UIPickerView()
    // MARK: - IBOUtlets
//    @IBOutlet weak var bottomScrollToSuperView: NSLayoutConstraint!
//    @IBOutlet weak var leftButton: UIButton!
//    @IBOutlet weak var imageView: UIImageView!
//    @IBOutlet weak var likeButton: UIButton!
//    @IBOutlet weak var rightButton: UIButton!
//    @IBOutlet weak var descriptionTextField: UITextField!
    
    
    
    @IBOutlet weak var newBackgroundImageView: UIImageView!
    
    
    @IBOutlet weak var bottomScrollToSuperView: NSLayoutConstraint!
    @IBOutlet weak var imageView: UIImageView!
    
    @IBOutlet weak var descriptionTextField: UITextField!
    
    @IBOutlet weak var trashButton: UIButton!
    
    
    @IBOutlet weak var likeButton: UIButton!
    
    
    
    // MARK: - lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        loadInfoFromUserDefaults()
        addRecognizers()
        registerForKeyboardNotifications()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.currentName = photoArray[currentIndex].name
        let imageName = self.photoArray[currentIndex].name
        self.imageView.image = loadImage(fileName: imageName)
     
        self.imageView.contentMode = .scaleAspectFit
        self.descriptionTextField.text = self.photoArray.first?.description
//        setLike()
//        self.leftButton.addGradientTwo(withRadius: 6, opacity: 0.5)
//        self.rightButton.addGradientTwo(withRadius: 6, opacity: 0.5)
//        self.rightButton.roundCorners(15)
//        self.leftButton.roundCorners(15)
//        self.rightButton.titleLabel?.textDropShadow(opacity: 0.5)
//        self.leftButton.titleLabel?.textDropShadow(opacity: 0.5)
        self.imageView.roundCorners(15)
        self.descriptionTextField.placeholder = "enter description PLEASE"
    //    self.rightButton.setImage(UIImage(named: "right"), for: .normal)
        let leftSwipe = UISwipeGestureRecognizer(target: self, action: #selector(leftSwipeRecognizer))
        leftSwipe.direction = .left
        self.newImageView.addGestureRecognizer(leftSwipe)
        self.newImageView.isUserInteractionEnabled
        let rightSwipe = UISwipeGestureRecognizer(target: self, action: #selector(rightSwipeRecognizer))
        rightSwipe.direction = .right
        self.newImageView.addGestureRecognizer(rightSwipe)
        self.imageView.isUserInteractionEnabled
        self.imageView.addGestureRecognizer(leftSwipe)
        self.imageView.addGestureRecognizer(rightSwipe)
        self.preparePicker()
//        self.pickerSlider.selectedRow(inComponent: self.currentIndex)
        self.pickerSlider.selectRow(self.currentIndex, inComponent: 0, animated: true)
     

        
    }
  
    func preparePicker(){
        self.rotationAngle = -90 * (.pi/180)
        self.pickerSlider.transform = CGAffineTransform(rotationAngle: rotationAngle)
        self.pickerSlider.delegate = self
        self.pickerSlider.frame.size = CGSize(width: self.view.frame.width + 300, height: 100)
        self.pickerSlider.frame.origin = CGPoint(x: self.view.frame.origin.x - 150 , y: self.view.frame.maxY -  self.pickerSlider.frame.height - 50 )
        self.view.addSubview(pickerSlider)
        
    }
    
    
    
    
    
    
    // MARK: - IBActions
    
    @IBAction func trashButtonPressed(_ sender: UIButton) {
        let alert = UIAlertController(title: "Removing photo", message: "Are you sure you want to remove the photo?", preferredStyle: .alert)
                let yesAction = UIAlertAction(title: "Yes", style: .default) { _ in
                    self.removeImageName()

                }
                let noAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
                alert.addAction(yesAction)
                alert.addAction(noAction)
                self.present(alert, animated: true, completion: nil)
 

    }
    @IBAction func leftSwipeRecognizer(_ recognizer: UISwipeGestureRecognizer) {
        self.photoArray[currentIndex].description = self.descriptionTextField.text
        var newImageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 0, height: 0  ))
        newImageView.frame.size =  self.imageView.frame.size
        newImageView.layer.cornerRadius = 15
        newImageView.frame.origin.y = self.imageView.frame.origin.y //+ (self.navigationController?.navigationBar.frame.height)! ?? 0
        newImageView.clipsToBounds = true
        newImageView.frame.origin = self.imageView.frame.origin
        newImageView.image = self.imageView.image
        newImageView.contentMode =  .scaleAspectFit
       
        self.view.addSubview(newImageView)
        
        self.imageView.image = previousImage()
        
        UIView.animate(withDuration: 1) {
            newImageView.frame.origin.x = 0 - newImageView.frame.width
            newImageView.frame.origin.y = self.imageView.frame.origin.y
        } completion: { _ in
            self.descriptionTextField.text = self.photoArray[self.currentIndex].description
            self.setLike()
            newImageView.removeFromSuperview()
            self.currentName = self.photoArray[self.currentIndex].name
            self.pickerSlider.selectRow(self.currentIndex, inComponent: 0, animated: true)

            
        }
        
    }
    @IBAction func tapDetected(_ recognizer: UITapGestureRecognizer){
        self.view.endEditing(true)
    }
    @IBAction func tapImageDetected(_ recognizer: UITapGestureRecognizer){
        self.view.endEditing(true)
        //        descriptionTextField.resignFirstResponder()
        self.newImageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 0, height: 0  ))
        self.newImageView.frame.size =  self.imageView.frame.size
        self.newImageView.clipsToBounds = true
        self.newImageView.layer.cornerRadius = 15
        self.newImageView.frame.origin = self.imageView.frame.origin
        self.newImageView.image = self.imageView.image
        self.newImageView.contentMode =  .scaleAspectFit
        self.view.addSubview(self.newImageView)
        UIView.animate(withDuration: 0.3, delay: 0, options: .curveLinear) {
            self.newImageView.frame.size = self.view.frame.size
            self.newImageView.frame.origin = self.view.frame.origin
            self.newImageView.backgroundColor = .black
        }
        self.newImageView.isUserInteractionEnabled = true
        let recognizerSmall = UITapGestureRecognizer(target: self, action: #selector(tapSmallRecognizer))
        self.newImageView.addGestureRecognizer(recognizerSmall)
        
        
    }
    @IBAction func tapSmallRecognizer(_ recognizer: UITapGestureRecognizer){
        UIView.animate(withDuration: 0.3, delay: 0, options: .curveLinear) {
            self.newImageView.frame.size = self.imageView.frame.size
            self.newImageView.frame.origin = self.imageView.frame.origin
        } completion: { _ in
            self.newImageView.backgroundColor = .clear
            
            self.newImageView.removeFromSuperview()
        }
        
    }
    
    
    
    
    
    @IBAction func rightSwipeRecognizer(_ recognizer: UITapGestureRecognizer) {
        
        self.photoArray[currentIndex].description = self.descriptionTextField.text
        let movingImageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
        movingImageView.frame.size = self.imageView.frame.size
        movingImageView.frame.origin.y = self.imageView.frame.origin.y
        movingImageView.layer.cornerRadius = 15
        movingImageView.clipsToBounds = true
        movingImageView.frame.origin = CGPoint(x: self.view.frame.width, y: self.imageView.frame.origin.y)
        movingImageView.image = nextImage()
        movingImageView.contentMode = .scaleAspectFit
        self.view.addSubview(movingImageView)
        UIView.animate(withDuration: 1) {
            movingImageView.frame.origin = self.imageView.frame.origin
            movingImageView.frame.origin.y = self.imageView.frame.origin.y
        } completion: { _ in
            self.imageView.image = movingImageView.image
            movingImageView.removeFromSuperview()
            self.descriptionTextField.text = self.photoArray[self.currentIndex].description
            self.setLike()
            self.currentName = self.photoArray[self.currentIndex].name
            self.pickerSlider.selectRow(self.currentIndex, inComponent: 0, animated: true)
        }
     //   deselectButtons()
      //  sender.isSelected = true
        
        
    }
    
    @IBAction func likeButtonPressed(_ sender: UIButton) {
        self.photoArray[currentIndex].like.toggle()
        updateInfoInUserDefaults()
        setLike()
        
    }
    
    @IBAction func textFieldUpdate(_ notification: NSNotification) {
        
    }
    
    @IBAction func keyboardWillShow(_ notification: NSNotification) {
        guard let userInfo = notification.userInfo,
              let animationDuration = (userInfo[UIResponder.keyboardAnimationDurationUserInfoKey] as? NSNumber)?.doubleValue,
              let keyboardScreenEndFrame = (userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue else { return }
        
        if notification.name == UIResponder.keyboardWillHideNotification {
            bottomScrollToSuperView.constant = 0
        }
        
        if notification.name == UIResponder.keyboardWillShowNotification {
            bottomScrollToSuperView.constant = keyboardScreenEndFrame.height + 15
        }
        
        view.needsUpdateConstraints()
        UIView.animate(withDuration: animationDuration) {
            self.view.layoutIfNeeded()
        }}
    
    
    
    // MARK: - flow func
    func removeImageName () {
        var trashImageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 0, height: 0  ))
          trashImageView.frame.size = self.imageView.frame.size
          trashImageView.layer.cornerRadius = 15
          trashImageView.frame.origin.y = self.imageView.frame.origin.y
          trashImageView.clipsToBounds = true
          trashImageView.frame.origin = self.imageView.frame.origin
          trashImageView.image = self.imageView.image
          trashImageView.contentMode = .scaleAspectFit
          self.view.addSubview(trashImageView)
          let index = currentIndex
          self.imageView.image = previousImage()
          UIView.animate(withDuration: 0.3, delay: 0, options: .curveLinear) {
              trashImageView.frame.origin = CGPoint(x: 0, y: 0)
              trashImageView.frame.size.width /= 20
              trashImageView.frame.size.height /= 20
          } completion: { _ in
              self.descriptionTextField.text = self.photoArray[self.currentIndex].description
              self.setLike()
              self.photoArray.removeAll { onePhoto in
                  return onePhoto.name == self.currentName
              }
              if self.currentIndex >= self.photoArray.count {
                  self.currentIndex = self.photoArray.count - 1
              }
              self.currentName = self.photoArray[self.currentIndex].name
              self.pickerSlider.reloadAllComponents()
              self.pickerSlider.selectRow(self.currentIndex, inComponent: 0, animated: true)
              self.updateInfoInUserDefaults()
              
              
          }
    }
    func setLike(){
        if self.photoArray[self.currentIndex].like {
            self.likeButton.setImage(UIImage(systemName: "heart.fill"), for: .normal)
        }
        if !self.photoArray[self.currentIndex].like {
            self.likeButton.setImage(UIImage(systemName: "heart"), for: .normal)
        }
    }
    
    func addRecognizers() {
        self.imageView.isUserInteractionEnabled = true
        self.imageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(tapImageDetected(_:))))
        
        let recognizer = UITapGestureRecognizer(target: self, action: #selector(tapDetected(_:)))
        self.view.addGestureRecognizer(recognizer)
    }
    
    private func registerForKeyboardNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(_:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(_:)), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
//    private func registerForTextFieldNotifications() {
//        NotificationCenter.default.addObserver(self, selector: #selector(textFieldUpdate(<#T##NSNotification#>)), , object: nil)
//    }
    func loadInfoFromUserDefaults () {
        guard let array = UserDefaults.standard.value([Photo].self, forKey: "photoArray") as? [Photo] else {return}
        photoArray = array
    }
    
    func updateInfoInUserDefaults () {
        UserDefaults.standard.set(encodable: photoArray, forKey: "photoArray")
    }
//
//    func deselectButtons(){
//        self.rightButton.isSelected = false
//        self.leftButton.isSelected = false
//    }
    
    func nextImage()-> UIImage? {
        currentIndex += 1
        if currentIndex >= self.photoArray.count {
            currentIndex = 0
        }
        return loadImage(fileName: self.photoArray[currentIndex].name)
        
    }
    func previousImage()-> UIImage? {
        currentIndex -= 1
        if currentIndex < 0 {
            currentIndex = self.photoArray.count - 1
        }
        return loadImage(fileName: self.photoArray[currentIndex].name)
    }
    
    
}


extension GalleryViewController : UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        //        textField.resignFirstResponder() //
        view.endEditing(true)
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        self.photoArray[currentIndex].description = self.descriptionTextField.text
        updateInfoInUserDefaults()
    }
    
}
extension GalleryViewController: UIPickerViewDataSource, UIPickerViewDelegate {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return photoArray.count
    }
    
    func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
        return 100
    }
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        let customImage = CustomImageView()
        customImage.configure(name: photoArray[row].name)
        self.rotationAngle =  90 * (.pi/180)
     customImage.transform = CGAffineTransform(rotationAngle: rotationAngle)
        return customImage
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int){
       
        currentIndex = row
        currentName =  photoArray[currentIndex].name
        imageView.image = loadImage(fileName: currentName)
        descriptionTextField.text = photoArray[currentIndex].description
        setLike()
    }

    
}

