//
//  ImageCollectionViewCell.swift
//  secretRoom
//
//  Created by Oleg Liakh on 15.02.22.
//

import UIKit

class ImageCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var customImageView: UIImageView!
    func configure(with photo : Photo) {
        customImageView.image = loadImage(fileName: photo.name)
    }
    func configureFirst ( ) {
        customImageView.image = UIImage(named: "plus")
        
    }
}
