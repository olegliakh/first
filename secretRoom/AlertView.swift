
import UIKit

class AlertView: UIView {
    
    //MARK : - lets/vars
//    let message : String
//    let buttonTitle : String
    
    //MARK : - iboutlets
    @IBOutlet weak var blurView: UIVisualEffectView!
    @IBOutlet weak var messageLabel: UILabel!
    
    @IBOutlet weak var actionButton: UIButton!
    @IBOutlet weak var centralAlertView: UIView!
    
    //MARK : - ibactions
   
    @IBAction func actionButtonPressed(_ sender: UIButton) {
        removeFromSuperview()
    }
    
    
    func prepareAlertWith(message: String, andTitle: String){
        bringSubviewToFront(centralAlertView)
        messageLabel.text = message
        actionButton.setTitle(andTitle, for: .normal)
        
        centralAlertView.layer.cornerRadius = 15
    }
    
    static func instanceFromNib() -> AlertView {
        guard let view = UINib(nibName: "AlertView", bundle: nil).instantiate(withOwner: nil, options: nil).first as? AlertView
        else {
            return AlertView()
        }
        return view
    }
    
}
