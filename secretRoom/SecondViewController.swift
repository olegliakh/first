//
//  SecondViewController.swift
//  secretRoom
//
//  Created by Oleg Liakh on 17.01.22.
//

import UIKit
// MARK: - class
class SecondViewController: UIViewController {
    // MARK: - lets/vars
    
    var photoArray : [Photo] = []
    
    // MARK: - IBOUtlets
    
  
    @IBOutlet weak var myCollectionView: UICollectionView!
    @IBOutlet weak var addNewPhoto: UIButton!
    @IBOutlet weak var nextVcButton: UIButton!
    @IBOutlet weak var plusButton: UIButton!
    @IBOutlet weak var secondImageView: UIImageView!
    
    // MARK: - lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        myCollectionView.delegate = self
       // self.addNewPhoto.addGradientTwo(withRadius: 5, opacity: 0.5)
      //  self.nextVcButton.addGradientTwo(withRadius: 5, opacity: 0.5)
        self.nextVcButton.titleLabel?.textDropShadow(opacity: 0.5)
        self.addNewPhoto.titleLabel?.textDropShadow(opacity: 0.5)
        
        if let array = UserDefaults.standard.value([Photo].self, forKey: "photoArray") {
            photoArray = array
            print(photoArray.count)
        }
        let addTitle =  "Add New".localized
        self.addNewPhoto.setTitle(addTitle, for: .normal)
        let nextTitle = "Next".localized
        self.nextVcButton.setTitle(nextTitle, for: .normal)
        
        
    }
    
    // MARK: - IBActions
    
    @IBAction func plusButtonPressed(_ sender: UIButton) {
    }
    
    
    @IBAction func addNewPhotoButtonPressed(_ sender: UIButton) {
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.allowsEditing = true
        imagePicker.sourceType = .photoLibrary
        imagePicker.modalPresentationStyle = .fullScreen
        present(imagePicker, animated: true, completion: nil)
        
    }
    
    
    @IBAction func nextVcButtonPressed(_ sender: UIButton) {
        print("1")
        guard let controller = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "GalleryViewController") as? GalleryViewController else {return}
        print("2")
        self.navigationController?.pushViewController(controller, animated: true)
        
    }
    
    
    // MARK: - flow func

}
extension SecondViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        var newImage = UIImage()
        if let image = info[.editedImage] as? UIImage {
            newImage = image
        }
        if let image = info[.originalImage] as? UIImage{
            newImage = image
        }
        
        
        guard let imageName = saveImage(newImage) else {return}
        let photo = Photo(name: imageName, description: nil, like: false)
        
       
        photoArray.append(photo)
        UserDefaults.standard.set(encodable:photoArray, forKey: "photoArray")
        picker.dismiss(animated: true, completion: nil)
    }
}
extension SecondViewController: UICollectionViewDelegate, UICollectionViewDataSource , UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return   photoArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CollectionViewCell", for: indexPath) as? CollectionViewCell else { return UICollectionViewCell () }
        cell.configure(with: photoArray[indexPath.item])
        print(photoArray[indexPath.item].name)
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 100, height: 100)
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print("Did select item")
//        guard let controller  = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "GalleryViewController") as? GalleryViewController else {return }
//        self.navigationController?.pushViewController(controller, animated: true)
        guard let controller = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "GalleryViewController") as? GalleryViewController else {return}
        print()
        self.navigationController?.pushViewController(controller, animated: true)
    }
    

    
}
