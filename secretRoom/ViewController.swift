//
//  ViewController.swift
//  secretRoom
//
//  Created by Oleg Liakh on 17.01.22.
//

import UIKit
import SwiftyKeychainKit

// MARK: - class
class ViewController: UIViewController {
    // MARK: - lets/vars
    let keychain = Keychain(service: "com.swifty.keychainkit")
    let accessTokenKey = KeychainKey<String>(key: "KeychainKey")
    
    var isFirstEnterance = true
    
    
    // MARK: - IBOUtlets
    
    @IBOutlet weak var labelSecretStart: UILabel!
    
    @IBOutlet weak var onePassword: UITextField!
    
    @IBOutlet weak var repeatPassword: UITextField!
    
    
    
    @IBOutlet weak var enterButton: UIButton!
    
    @IBOutlet weak var confirmButton: UIButton!
    
    
    
    // MARK: - lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        if let password = //UserDefaults.standard.value(forKey: "password")
            try? keychain.get(accessTokenKey)
        {
            isFirstEnterance = false
        }
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if isFirstEnterance {
            self.enterButton.isHidden = true
        }
        if !isFirstEnterance {
            self.confirmButton.isHidden = true
            self.repeatPassword.isHidden = true
            self.enterButton.isHidden = false
        }
        self.onePassword.isSecureTextEntry = true
        self.repeatPassword.isSecureTextEntry = true
        self.enterButton.roundCorners(15)
        self.confirmButton.roundCorners(15)
        self.labelSecretStart.roundCorners(15)
        self.enterButton.dropShadow(colors: UIColor.white.cgColor, opacity: 0.2)
        self.labelSecretStart.dropShadow(colors: UIColor.blue.cgColor, opacity: 0.2)
        self.onePassword.dropShadow(colors: UIColor.white.cgColor, opacity: 0.5)
        self.repeatPassword.dropShadow(colors: UIColor.white.cgColor, opacity: 0.5)
        // self.enterButton.dropShadow(colors: UIColor.white.cgColor, opacity: 0.5)
        // self.confirmButton.dropShadow(colors: UIColor.white.cgColor, opacity: 0.5)
        self.enterButton.titleLabel?.textDropShadow(opacity: 1)
        self.confirmButton.titleLabel?.textDropShadow(opacity: 1)
        let oneTitle = "Enter".localized
        self.enterButton.setTitle(oneTitle, for: .normal)
        let twoTitle = "Confirm".localized
        self.confirmButton.setTitle(twoTitle, for: .normal)
        
    }
    
    
    
    // MARK: - IBActions
    
    
    @IBAction func enterButtonPressed(_ sender: UIButton) {
        if let text = self.onePassword.text{
            if text.isEmpty{
                //                showCustomAlert(withMessage: "Enter password", andTitle: "OK")
            }
            guard let password = try? keychain.get(accessTokenKey) as? String else {return}
            if text == password {
                guard   let controller = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "CollectionViewController") as? CollectionViewController else {return}
                
                self.navigationController?.pushViewController(controller, animated: true)
                
            }
            else {
                showCustomAlert(withMessage: "Incorrect password", andTitle: "OK")
            }
        }
        
    }
    
    
    @IBAction func confirmButtonPressed(_ sender: UIButton) {
        if let text = self.onePassword.text{
            if text.isEmpty{
                showCustomAlert(withMessage: "Enter and confirm password", andTitle: "OK")
            }
            else if self.onePassword.text != self.repeatPassword.text {
                showCustomAlert(withMessage: "Enter and confirm password", andTitle: "OK")
            }
            
            else  if self.onePassword.text == self.repeatPassword.text {
                if let password = self.onePassword.text {
                    try? keychain.set(password, for: accessTokenKey)
                    
                }
            }
        }
    }
    // MARK: - flow func
    
    func attribText () {
        let myString = "Start"
        let attributes : [NSAttributedString.Key : Any] = [
            .font: UIFont(name: "Bicubik", size: 24)
        ]
        let attributedString = NSAttributedString(string: myString, attributes: attributes)
        self.enterButton.setAttributedTitle(attributedString, for: .normal)
    }
    
    
    func showCustomAlert(withMessage: String, andTitle: String){
        let alert = AlertView.instanceFromNib()
        alert.prepareAlertWith(message: withMessage, andTitle: andTitle)
        alert.frame = self.view.bounds
        alert.center = self.view.center
        self.view.addSubview(alert)
    }
}

