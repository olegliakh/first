//
//  CollectionViewController.swift
//  secretRoom
//
//  Created by Oleg Liakh on 15.02.22.
//

import UIKit

class CollectionViewController: UIViewController {
    

    var photoArray : [Photo] = []
    @IBOutlet weak var collectionView: UICollectionView!
    override func viewDidLoad() {
        super.viewDidLoad()

        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.loadPhotos()
        self.collectionView.reloadData()
        
    }
    func loadPhotos(){
        if let array = UserDefaults.standard.value([Photo].self, forKey: "photoArray") {
            photoArray = array
        }
    }

}

extension CollectionViewController: UICollectionViewDelegate, UICollectionViewDataSource , UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return   photoArray.count + 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ImageCollectionViewCell", for: indexPath) as? ImageCollectionViewCell else { return UICollectionViewCell () }
        if indexPath.item == 0 {
            cell.configureFirst()
        } else {
            cell.configure(with: photoArray[indexPath.item - 1 ] )
        }
        
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 100, height: 100)
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print("Did select item")
        if indexPath.item == 0 {
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.allowsEditing = true
            imagePicker.sourceType = .photoLibrary
            imagePicker.modalPresentationStyle = .fullScreen
            present(imagePicker, animated: true, completion: nil)
            
        } else {
        
        guard let controller = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "GalleryViewController") as? GalleryViewController else {return}
        controller.currentIndex = indexPath.item - 1
        print()
        self.navigationController?.pushViewController(controller, animated: true)
        
    }
    

    
}
}
extension CollectionViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        var newImage = UIImage()
        if let image = info[.editedImage] as? UIImage {
            newImage = image
        }
        else if let image = info[.originalImage] as? UIImage{
            newImage = image
        }
        
        
        guard let imageName = saveImage(newImage) else {return}
        let photo = Photo(name: imageName, description: nil, like: false)
        
       
        photoArray.insert(photo, at: 0)
        UserDefaults.standard.set(encodable:photoArray, forKey: "photoArray")
        picker.dismiss(animated: true, completion: nil)
    }
}
